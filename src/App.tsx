import Button from './components/Button';
import CurrencySelector from './components/CurrencySelector';

import './App.css';

import Logo from './assets/images/probemas.png';
import burgerMenu from './assets/icons/burger-menu.svg';
import burgerMenuClose from './assets/icons/burger-menu_close.svg';

function App() {
  const navigationItems = ['OSRS Gold', 'RS3 Gold', 'Sell RS Gold', 'OSRS Items', 'OSRS Accounts', 'Reward Chests'];

  return (
    <div className="App">
      <header className="Header">
        <div className="Header__burger-menu">
          <img className="Header__burger-menu__button" src={burgerMenu} alt="" />
          <div className="Header__burger-menu__menu">
            <img className="Header__burger-menu__close-button" src={burgerMenuClose} alt="" />

            <div>
              <div className="Header__mobile-navigation">
                {navigationItems.map((item) => (
                  <span className="Header__navigation__item" key={item}>
                    {item}
                  </span>
                ))}
              </div>
              <CurrencySelector />
              <div className="Header__auth-buttons">
                <Button variant="outlined">Sign Up</Button>
                <Button variant="contained">Login</Button>
              </div>
            </div>
          </div>
        </div>
        <a href="/">
          <img className="Header__logo" src={Logo} alt="Probemas" />
        </a>
        <nav className="Header__navigation">
          {navigationItems.map((item) => (
            <Button variant="text" key={item}>
              {item}
            </Button>
          ))}
        </nav>
        <div className="Header__right">
          <CurrencySelector />
          <Button variant="text">Sign Up</Button>
          <Button variant="contained">Login</Button>
        </div>
      </header>
    </div>
  );
}

export default App;
