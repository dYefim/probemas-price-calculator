/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import clsx from 'clsx';

import './Button.css';

interface Props extends React.HTMLAttributes<HTMLButtonElement> {
  variant?: 'contained' | 'outlined' | 'text';
}

function Button({ className, variant = 'contained', ...props }: Props) {
  return <button className={clsx('Button', variant, className)} type="button" {...props} />;
}

Button.defaultProps = {
  variant: 'contained',
};

export default Button;
