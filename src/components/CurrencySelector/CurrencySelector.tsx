import { useState } from 'react';
import clsx from 'clsx';

import CurrencySelectorButton from './CurrencySelectorButton';
import CURRENCIES from './currencies';

import './CurrencySelector.css';
import CurrencyOption from './CurrencyOption';

function CurrencySelector() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedCurrency, setSelectedCurrency] = useState('USD');

  const selectCurrency = (currency: string) => {
    setSelectedCurrency(currency);
    setIsOpen(false);
  };

  return (
    <div className={clsx('CurrencySelector', isOpen && 'open')}>
      <CurrencySelectorButton currency={selectedCurrency} setIsOpen={setIsOpen} />
      {isOpen && (
        <div className="CurrencySelector__options">
          {Object.entries(CURRENCIES).map(([currencyName, currencyStats]) => (
            <CurrencyOption
              currencyName={currencyName}
              selectCurrency={selectCurrency}
              icon={currencyStats.icon}
              isSelected={currencyName === selectedCurrency}
              key={currencyName}
            />
          ))}
        </div>
      )}
    </div>
  );
}

export default CurrencySelector;
