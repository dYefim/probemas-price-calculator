import clsx from 'clsx';

interface Props {
  currencyName: string;
  selectCurrency: (currency: string) => void;
  isSelected: boolean;
  icon: string;
}

function CurrencyOption({ currencyName, selectCurrency, isSelected, icon }: Props) {
  const select = () => selectCurrency(currencyName);

  return (
    <span
      className={clsx('CurrencySelector__option', currencyName, isSelected && 'selected')}
      onClick={select}
      onKeyDown={select}
      role="option"
      aria-selected={isSelected}
      tabIndex={0}
      key={currencyName}
      data-flag={currencyName}
    >
      <img src={icon} alt="" /> {currencyName}
    </span>
  );
}

export default CurrencyOption;
