import auFlag from '../../assets/icons/flags/au.svg';
import caFlag from '../../assets/icons/flags/ca.svg';
import euFlag from '../../assets/icons/flags/eu.svg';
import gbFlag from '../../assets/icons/flags/gb.svg';
import usFlag from '../../assets/icons/flags/us.svg';

type Currency = { price: number; icon: string };

const CURRENCIES: Record<string, Currency> = {
  USD: { price: 1, icon: usFlag },
  EUR: { price: 1, icon: euFlag },
  GBP: { price: 1, icon: gbFlag },
  AUD: { price: 1, icon: auFlag },
  CAD: { price: 1, icon: caFlag },
};

export default CURRENCIES;
