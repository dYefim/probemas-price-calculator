import clsx from 'clsx';

import CURRENCIES from './currencies';

import ArrowDown from '../../assets/icons/arrow-down';

import './CurrencySelector.css';

interface Props {
  currency: string;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

function CurrencySelectorButton({ currency, setIsOpen }: Props) {
  const toggle = () => setIsOpen((open) => !open);

  return (
    <span
      className={clsx('CurrencySelector__button')}
      onClick={toggle}
      onKeyDown={toggle}
      tabIndex={0}
      role="switch"
      aria-checked={false}
    >
      <img src={CURRENCIES[currency].icon} data-flag={currency} alt="" />
      <span className="CurrencySelector__active-currency">{currency}</span>
      <ArrowDown className="CurrencySelector__arrow" />
    </span>
  );
}

export default CurrencySelectorButton;
