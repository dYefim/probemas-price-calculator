// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
function ArrowDown({ className }: { className: string }) {
  return (
    <svg
      className={className}
      width="16"
      height="17"
      viewBox="0 0 16 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M12 7L8 11L4 7" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
}

export default ArrowDown;
